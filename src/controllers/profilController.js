const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const getProfil = async (req, res) => {
    let profils = await prisma.profil.findMany({
        include: {
            user: {
                select: {
                    username: true,
                    email: true,
                },
            },
        },
    });
    if (profils.length > 0) {
        res.json(profils);
    } else {
        res.json({ message: "Data Masih Kosong!" });
    }
};

const getProfilById = async (req, res) => {
    let { id } = req.params;
    let profil = await prisma.profil.findUnique({
        where: {
            id: isNaN(parseInt(id)) ? 0 : parseInt(id),
        },
        include: {
            user: {
                select: {
                    username: true,
                    email: true,
                },
            },
        },
    });
    if (profil) {
        res.json(profil);
    } else {
        res.status(404).json({ message: "Data Tidak Ditemukan" });
    }
};

const createProfil = async (req, res) => {
    let { firstName, lastName, profilePict, alamat, userId } = req.body;
    try {
        let validUserId = await prisma.profil.findUnique({
            where: { userId: Number(userId) },
        });
        if (!validUserId) {
            if (firstName && lastName && profilePict && alamat && userId) {
                const profil = await prisma.profil.create({
                    data: {
                        firstName,
                        lastName,
                        profilePict,
                        alamat,
                        userId,
                    },
                });
                res.json({ message: "Profil Berhasil Ditambahkan!", profil });
            } else {
                res.status(400).json({ message: "Semua Data Wajib Diisi!" });
            }
        } else {
            res.status(404).json({ message: "User Id Sudah Digunakan" });
        }
    } catch (error) {
        res.json({
            message: `Error! ${error.meta.field_name} Tidak Ditemukan`,
        });
    }
};

const updateProfil = async (req, res) => {
    let { firstName, lastName, profilePict, alamat, userId } = req.body;
    let { id } = req.params;
    try {
        const profil = await prisma.profil.update({
            where: { id: isNaN(parseInt(id)) ? 0 : parseInt(id) },
            data: { firstName, lastName, profilePict, alamat, userId },
        });
        res.json({ message: "Profil Berhasil Diubah!", profil });
    } catch (error) {
        res.json({ message: "Error!" });
    }
};

const deleteProfil = async (req, res) => {
    let { id } = req.params;
    try {
        const profil = await prisma.profil.delete({
            where: { id: isNaN(parseInt(id)) ? 0 : parseInt(id) },
        });
        res.json({ message: "Data Profil Berhasil Dihapus!", profil });
    } catch (error) {
        res.json({ message: "Error!" });
    }
};

module.exports = {
    getProfil,
    getProfilById,
    createProfil,
    updateProfil,
    deleteProfil,
};
