
const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()

const getChat = async (req, res) => {
  let {userId} = req.user
  const chat = await prisma.$queryRaw`
    SELECT user.id, user.username FROM chat
    JOIN user ON penerimaId = user.id
    WHERE
    user.id != ${userId}
    AND
    (pengirimId = ${userId} OR penerimaId = ${userId})
    GROUP BY user.id
    `
  chat.length? res.json(chat) : res.send("Data masih kosong")
}
const getChatById = async (req, res) => {
  let {id} = req.params
  let {userId} = req.user

       try {
     const chat = await prisma.$queryRaw`
     SELECT *  FROM (
      SELECT chat.id, isi, pengirimId, penerimaId, created_at FROM chat
      JOIN user ON pengirimId=user.id
      WHERE (pengirimId= ${userId} AND penerimaId= ${id}) 
      GROUP BY chat.id 
      UNION
      SELECT chat.id, isi, pengirimId, penerimaId, created_at FROM chat
      JOIN user ON penerimaId=user.id
      WHERE (pengirimId=${id} AND penerimaId=${userId}) 
      GROUP BY chat.id 
  ) as d ORDER BY created_at ASC`
     chat.length? res.json(chat) : res.send("Data masih kosong")
   } catch (error) {
    res.json(error.message)
   }
 
}


// const getChat = async (req, res) => {
//   let {userId} = req.user
//   console.log(userId)
//   const chat = await prisma.$queryRaw`
//     SELECT public.user.id, public.user.username FROM chat
//     JOIN public.user ON chat."penerimaId" = public.user.id
//     WHERE
//     public.user.id != ${userId}
//     AND
//     (chat."pengirimId" = ${userId} OR chat."penerimaId" = ${userId})
//     GROUP BY public.user.id
//     `
//     chat.length? res.json(chat) : res.send("Data masih kosong")

// }
// const getChatById = async (req, res) => {
//   let {id} = req.params
//   let {userId} = req.user
//        try {
//      const chat = await prisma.$queryRaw`
//         SELECT *  FROM (
//       SELECT chat."id", chat."isi", chat."pengirimId", chat."penerimaId", chat."created_at" FROM chat
//       JOIN public.user ON chat."pengirimId" = public.user.id
//       WHERE (chat."pengirimId" = ${userId} AND chat."penerimaId" = ${Number(id)})
//       GROUP BY chat."id" 
//       UNION
//       SELECT chat."id", chat."isi", chat."pengirimId", chat."penerimaId", chat."created_at" FROM chat
//       JOIN public.user ON chat."penerimaId" = public.user.id
//       WHERE (chat."pengirimId" = ${Number(id)} AND chat."penerimaId" = ${userId})
//       GROUP BY chat.id 
//   ) as d ORDER BY created_at ASC`
//    chat.length? res.json(chat) : res.send("Data masih kosong")

//    } catch (error) {
//     res.json(error.message)
//    }
// }
const postChat = async (req, res) => {
    let {userId} = req.user
    let {isi, penerimaId} = req.body;

      try {
        const chat =  await prisma.chat.create({
          data: {
           isi,
           pengirimId : Number(userId),
           penerimaId : Number(penerimaId)
          }
        })
        res.json({message : "Chat Berhasil ditambahkan", data : chat})
      } catch (error) {
        res.send(error.message)
      }
}

const updateChat = async (req, res) => {
    let {isi, penerimaId}= req.body
    let {id}= req.params
    let {userId} = req.user

    try {
        const chat = await prisma.chat.update({
        where: {
          id: Number(id)
        },
        data: {
          isi,
          penerimaId : Number(penerimaId),
        }
      })
      res.send({message: 'Chat Berhasil diupdate', chat})
        } catch (error) {
      res.send({message: 'Chat gagal diupdate', error})
  }
}

  const deleteChat = async (req, res) => {
    let {id}= req.params
    let {userId} = req.user

    try {
      const chat = await prisma.chat.deleteMany({
        where: {
          AND: [
            {
              id: Number(id)
           },
            {
              pengirimId: Number(userId)
            }
          ]
        }
      })
      chat.count? res.send("Chat berhasil dihapus") : res.send("Chat gagal dihapus.")
    } catch (error) {
      res.send(error)
    }
  }


module.exports = {
    getChat,
    getChatById,
    postChat,
    deleteChat,
    updateChat
}