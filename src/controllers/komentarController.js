const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const getKomentar = async (req, res) => {
    let komentars = await prisma.komentar.findMany({
        include: {
            replyKomentar: true,
        },
    });
    if (komentars.length > 0) {
        res.json(komentars);
    } else {
        res.json({ message: "Data Masih Kosong!" });
    }
};

const getKomentarById = async (req, res) => {
    let { id } = req.params;
    let komentar = await prisma.komentar.findUnique({
        where: {
            id: isNaN(parseInt(id)) ? 0 : parseInt(id),
        },
        include: {
            replyKomentar: true,
        },
    });
    if (komentar) {
        res.json(komentar);
    } else {
        res.status(404).json({ message: "Data Tidak Ditemukan" });
    }
};

const createKomentar = async (req, res) => {
    let { isi, userId, reviewId } = req.body;
    try {
        if (isi && userId && reviewId) {
            const komentar = await prisma.komentar.create({
                data: {
                    isi,
                    userId,
                    reviewId,
                },
            });
            res.json({ message: "Komentar Berhasil Ditambahkan!", komentar });
        } else {
            res.status(400).json({ message: "Semua Data Wajib Diisi!" });
        }
    } catch (error) {
        res.json({ message: "Error!", error });
    }
};

const updateKomentar = async (req, res) => {
    let { isi, userId, reviewId } = req.body;
    let { id } = req.params;
    try {
        const komentar = await prisma.komentar.update({
            where: { id: isNaN(parseInt(id)) ? 0 : parseInt(id) },
            data: { isi, userId, reviewId },
        });
        res.json({ message: "Komentar Berhasil Diubah!", komentar });
    } catch (error) {
        res.json({ message: "Error!" });
    }
};

const deleteKomentar = async (req, res) => {
    let { id } = req.params;
    try {
        const komentar = await prisma.komentar.delete({
            where: { id: isNaN(parseInt(id)) ? 0 : parseInt(id) },
        });
        res.json({ message: "Data Komentar Berhasil Dihapus!", komentar });
    } catch (error) {
        res.json({ message: "Error!" });
    }
};

module.exports = {
    getKomentar,
    getKomentarById,
    createKomentar,
    updateKomentar,
    deleteKomentar,
};
