const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const getReview = async (req, res) => {
    const review = await prisma.review.findMany();
    review.length? res.json(review) : res.send("Data masih kosong")

};
const getReviewById = async (req, res) => {
    let { id } = req.params;
    try {
        const review = await prisma.review.findUnique({
            where: {
                id: Number(id),
            },
        });
        review? res.json(review) : res.send("Data masih kosong")
    } catch (error) {
        res.json(error.message);
    }
};
const postReview = async (req, res) => {
    let { rating, isi, bukuId, userId } = req.body;
    if (rating <= 10) {
        try {
            const review = await prisma.review.create({
                data: {
                    rating,
                    isi,
                    bukuId,
                    userId,
                },
            });
            res.json({ message: "Review Berhasil ditambahkan", data: review });
        } catch (error) {
            res.send(error.message);
        }
    } else {
        return res
            .status(400)
            .json({ message: "Batas rating review hanya sampai 10" });
    }
};

const updateReview = async (req, res) => {
    let { rating, isi, bukuId, userId } = req.body;
    let { id } = req.params;
    try {
        const review = await prisma.review.update({
            where: {
                id: Number(id),
            },
            data: {
                rating,
                isi,
                bukuId,
            },
        });
        res.send({ message: "Review Berhasil diupdate", review });
    } catch (error) {
        res.send({ message: "Review gagal diupdate", error });
    }
};

const deleteReview = async (req, res) => {
    let { id } = req.params;

    try {
        const review = await prisma.review.delete({
            where: {
                id: Number(id),
            },
        });
        res.json("review Berhasil dihapus");
    } catch (error) {
        res.send(error);
    }
};

module.exports = {
    getReview,
    getReviewById,
    postReview,
    deleteReview,
    updateReview,
};
