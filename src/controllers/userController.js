const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const getUser = async (req, res) => {
    let users = await prisma.user.findMany();
    if (users.length > 0) {
        res.json(users);
    } else {
        res.json({ message: "Data Masih Kosong!" });
    }
};

const getUserById = async (req, res) => {
    let { id } = req.params;
    let user = await prisma.user.findUnique({
        where: {
            id: isNaN(parseInt(id)) ? 0 : parseInt(id),
        },
    });
    if (user) {
        res.json(user);
    } else {
        res.status(404).json({ message: "Data Tidak Ditemukan" });
    }
};

const updateUser = async (req, res) => {
    let { username, email, password } = req.body;
    let { id } = req.params;
    try {
        const user = await prisma.user.update({
            where: { id: isNaN(parseInt(id)) ? 0 : parseInt(id) },
            data: { username, email, password },
        });
        res.json({ message: "User Berhasil Diubah!", user });
    } catch (error) {
        res.json({ message: "Error!" });
    }
};

const deleteUser = async (req, res) => {
    let { id } = req.params;
    try {
        const user = await prisma.user.delete({
            where: { id: isNaN(parseInt(id)) ? 0 : parseInt(id) },
        });
        res.json({ message: "Data User Berhasil Dihapus!", user });
    } catch (error) {
        res.json({ message: "Error!" });
    }
};

module.exports = {
    getUser,
    getUserById,
    updateUser,
    deleteUser,
};
