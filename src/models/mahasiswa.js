class Mahasiswa{
  constructor(id, nama){
    this.id = id
    this.nama = nama
  }
}
class Nilai{
  constructor(id, index, skor){
    this.id = id
    this.mahasiswa = null
    this.matakuliah = null
    this.skor = skor
    this.index = index
  }
}
class MataKuliah{
  constructor(id, nama){
    this.id = id
    this.nama = nama
    this.created_at = null
    this.updated_at = null
  }
}

module.exports = {
    Mahasiswa,
    Nilai,
    MataKuliah
}